CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Infobar is a simple module block that lets you to show some
information as a sticky notification on your web page.
The information is configurable in a block.

To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/infobar


REQUIREMENTS
------------

No special requirements


INSTALLATION
------------

Install as you would normally install a contributed Drupal. See:
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.


CONFIGURATION
-------------

* Go to admin/structure/block

   - Click "Place block" on your favorite region.
     Usually one of the "Footer" region.

   - Search for "Infobar block" and click "Place Block"

   - In the "Information" field add some information

   - You might want to uncheck "Display title"

   - Click "Save block"

* Go to your site

   - You can see the information block appearing


MAINTAINERS
-----------

Current maintainers:
 * gopisathya - https://www.drupal.org/user/159947
