<?php

namespace Drupal\infobar\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'InfobarBlock' block.
 *
 * @Block(
 *  id = "infobar_block",
 *  admin_label = @Translation("Infobar block"),
 * )
 */
class InfobarBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['information'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Information'),
      '#description' => $this->t('Write the information here'),
      '#default_value' => $this->configuration['information'],
      '#weight' => '2',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['information'] = $form_state->getValue('information');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'infobar',
      '#information' => $this->configuration['information'],
    ];
  }

}
